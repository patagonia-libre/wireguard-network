FROM alpine:3

LABEL MAINTAINER="patagonialibre.ar"

ENV RELEASE "v1.0.20210424"

RUN apk --no-cache update && apk add --no-cache \
    bc \
    build-base\
    curl \
    #dkms \
	git \
	gnupg \ 
	ifupdown \
	iproute2 \
	iptables \
	#iputils-ping \
	jq \
	libc6-compat \
	#libelf-dev \
	net-tools \ 
    openresolv \ 
    perl \
	pkgconfig \
    libqrencode \
    gcc g++ libcurl \
    linux-headers
WORKDIR /app
RUN cd /app && \
 git clone https://git.zx2c4.com/wireguard-linux-compat && \
 git clone https://git.zx2c4.com/wireguard-tools && \
 cd wireguard-tools && \
 git checkout "${RELEASE}" && \
 make -C src -j$(nproc) && \
 make -C src install

COPY /default-config /
EXPOSE 51820/udp
#CMD ["wg-quick", "up", "wg0"]
