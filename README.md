# WireGuardNetwork
Hola! estamos trabajando en este proyecto.

Las principales tareas que nos esperan son las siguientes:
- generar un ambiente de wireguard completo, que sea funcional y que fácilmente pueda iniciarse. Para esto es necesario completar:
    - generar claves del servidor
    - generar claves de los peers
    - verificar correcto funcionamiento de wireguard
- abstraer al usuario de ciertas reglas técnicas de iptables. Por ejemplo, si quisiera exponer el puerto 80 de su servidor y mapearlo a un peer X con puerto Y.

